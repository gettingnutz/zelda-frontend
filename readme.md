
# Test technique o'clock

Ce repository contient les codes sources de l'application FrontEnd du test technique Zelda.\s\s
Auteur : Frédérik ISTACE\s\s
Date de création : 06/02/2020

## Stack technique
| Composant |Technologie  |
|--|--|
| Frontend |React.JS  |

## Restauration du code
> git clone https://gitlab.com/gettingnutz/zelda-frontend.git
> npm install
> npm start